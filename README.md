# Configuration

- PROXY_MIN_WAIT        (Integer, in seconds, default 1). Sleep at least N seconds before send a new request on a host
- PROXY_RANDOM_WAIT_USE (Boolean, "true/false", default false). By default, the algo use PROXY_MIN_WAIT
- PROXY_MIN_RANDOM_WAIT (Integer, in second, default 1)
- PROXY_MAX_RANDOM_WAIT (Integer, in second, default 3)
- PROXY_MULTI_WAIT      (Integer, default 1). It multiply the delay (req_end - req_begin * VAL)
- TH_CHANGE_AGENT       (Integer, default 100). % of chance to change the user_agent and reset the cookies.
